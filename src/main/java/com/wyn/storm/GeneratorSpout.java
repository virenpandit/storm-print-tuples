package com.wyn.storm;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import backtype.storm.tuple.Fields;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class GeneratorSpout extends BaseRichSpout {
	private static final Logger LOG = LoggerFactory.getLogger(GeneratorSpout.class);

	private SpoutOutputCollector collector;

	@SuppressWarnings("rawtypes")
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
	}

	public void nextTuple() {
        
        LOG.debug("Generating tuple");

        Values values = new Values("id-01", "Name-003");
        this.collector.emit(values);

		Utils.sleep(1000);
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("id", "name"));
	}
}
