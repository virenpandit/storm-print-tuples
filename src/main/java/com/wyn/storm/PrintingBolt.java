package com.wyn.storm;

import java.io.Serializable;
import java.util.*;

import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import backtype.storm.task.OutputCollector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class PrintingBolt extends BaseRichBolt {

	private static final Logger LOG = LoggerFactory.getLogger(PrintingBolt.class);

    OutputCollector _collector;

    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
    }

    public void execute(Tuple tuple) {
        // _collector.emit(tuple, new Values(tuple.getString(0) + "!!!"));
        LOG.debug("PrintingBolt.execute(): ID: " + tuple.getValueByField("id") + ", NAME: " + tuple.getValueByField("name"));
        _collector.ack(tuple);
    }

    public void cleanup() {
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        // declarer.declare(new Fields("word"));
    }

    public Map getComponentConfiguration() {
        return null;
    }
}
